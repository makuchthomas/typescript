export const ServiceRelations = {
    ["BlurayPackage"]: [
        "VideoRecording"
    ],
    ["TwoDayEvent"]: [
        "VideoRecording",
        "Photography"
    ]
}