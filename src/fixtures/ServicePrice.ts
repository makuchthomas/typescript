import { IServiceCost, IServiceDiscount } from "../models/ServiceCost"

export const ServicePrices: IServiceCost[] = [
   { serviceType: "Photography", serviceYear: 2020, servicePrice: 1700 },
   { serviceType: "Photography", serviceYear: 2021, servicePrice: 1800 },
   { serviceType: "Photography", serviceYear: 2022, servicePrice: 1900 },
   { serviceType: "VideoRecording", serviceYear: 2020, servicePrice: 1700 },
   { serviceType: "VideoRecording", serviceYear: 2021, servicePrice: 1800 },
   { serviceType: "VideoRecording", serviceYear: 2022, servicePrice: 1900 },
   { serviceType: "WeddingSession", serviceYear: 2020, servicePrice: 600 },
   { serviceType: "WeddingSession", serviceYear: 2021, servicePrice: 600 },
   { serviceType: "WeddingSession", serviceYear: 2022, servicePrice: 600 },
   { serviceType: "BlurayPackage", serviceYear: 2020, servicePrice: 300 },
   { serviceType: "BlurayPackage", serviceYear: 2021, servicePrice: 300 },
   { serviceType: "BlurayPackage", serviceYear: 2022, servicePrice: 300 },
   { serviceType: "TwoDayEvent", serviceYear: 2020, servicePrice: 400 },
   { serviceType: "TwoDayEvent", serviceYear: 2021, servicePrice: 400 },
   { serviceType: "TwoDayEvent", serviceYear: 2022, servicePrice: 400 },
];

export const ServiceDiscounts: IServiceDiscount[] = [
   { discountName: "WeddingSessionVideoPhotography", discountValue: 300 },
   { discountName: "WeddingSessionPhotography", discountValue: 600 },
   { discountName: "PhotographyVideoRecording2020", discountValue: 1200 },
   { discountName: "PhotographyVideoRecording2021", discountValue: 1300 },
   { discountName: "PhotographyVideoRecording2022", discountValue: 1300 }
]