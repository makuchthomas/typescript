import { ServiceType } from "./fixtures/ServiceType";
import { ServiceYear } from "./fixtures/ServiceYear";
import { ServiceDiscounts, ServicePrices } from "./fixtures/ServicePrice";
import { ServiceRelations } from "./fixtures/ServiceRelations";

export const updateSelectedServices = (
    previouslySelectedServices: ServiceType[],
    action: { type: "Select" | "Deselect"; service: ServiceType }
) => {
    switch (action.type) {
        case "Select": {
            if (previouslySelectedServices.includes(action.service)) {
                return previouslySelectedServices;
            }
            let requirements = ServiceRelations[action.service]?.some(x => previouslySelectedServices.includes(x)) ?? true;

            if (requirements)
                return [...previouslySelectedServices, action.service];
            return previouslySelectedServices;
        }
        case "Deselect": {
            if (!previouslySelectedServices.includes(action.service)) {
                return previouslySelectedServices;
            }
            previouslySelectedServices = previouslySelectedServices.filter(x => x !== action.service)

            previouslySelectedServices.filter(x => x !== action.service).forEach(st => {
                let requirements = ServiceRelations[st]?.some(y => previouslySelectedServices.includes(y)) ?? true;

                if (!requirements)
                    previouslySelectedServices = previouslySelectedServices.filter(pst => pst !== st)
            })

            return previouslySelectedServices;
        }
        default:
            return previouslySelectedServices;
    }
};

export const calculatePrice = (selectedServices: ServiceType[], selectedYear: ServiceYear) => {
    let price = 0;
    let discount = 0;

    selectedServices.forEach(service => {
        price += ServicePrices.filter(sp => sp.serviceYear === selectedYear
            && sp.serviceType === service)[0].servicePrice;
    });

    if (selectedServices.some(x => x === "WeddingSession") && ((selectedServices.some(x => x === "VideoRecording"))
        || ((selectedServices.some(x => x === "Photography") && (selectedYear === 2020 || selectedYear === 2021)))))
        discount = ServiceDiscounts.filter(e => e.discountName === "WeddingSessionVideoPhotography")[0].discountValue;

    if (selectedServices.some(x => x === "WeddingSession") && selectedServices.some(x => x === "Photography") && selectedYear === 2022)
        discount = ServiceDiscounts.filter(e => e.discountName === "WeddingSessionPhotography")[0].discountValue;

    if (selectedServices.some(x => x === "VideoRecording") && selectedServices.some(x => x === "Photography"))
        discount = ServiceDiscounts.filter(x => x.discountName === ("PhotographyVideoRecording" + selectedYear.toString()))[0].discountValue;

    if (selectedServices.some(x => x === "VideoRecording") && selectedServices.some(x => x === "Photography") && selectedServices.some(x => x === "WeddingSession"))
        discount = ServiceDiscounts.filter(x => x.discountName === ("PhotographyVideoRecording" + selectedYear.toString()))[0].discountValue +
            ServiceDiscounts.filter(x => x.discountName === ("WeddingSessionVideoPhotography"))[0].discountValue;

    if (selectedServices.some(x => x === "VideoRecording") && selectedServices.some(x => x === "Photography") && selectedServices.some(x => x === "WeddingSession")
        && selectedYear === 2022)
        discount = ServiceDiscounts.filter(x => x.discountName === ("PhotographyVideoRecording" + selectedYear.toString()))[0].discountValue +
            ServiceDiscounts.filter(x => x.discountName === ("WeddingSessionPhotography"))[0].discountValue;

    return { basePrice: price, finalPrice: price - discount };
};