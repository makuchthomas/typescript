import { ServiceType } from "../fixtures/ServiceType"
import { ServiceYear } from "../fixtures/ServiceYear"

export interface IServiceCost {
    serviceType: ServiceType,
    serviceYear: ServiceYear,
    servicePrice: number
}

export interface IServiceDiscount {
    discountName: string,
    discountValue: number,
}